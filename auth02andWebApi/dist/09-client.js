"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var https_1 = __importDefault(require("https"));
var dotenv_1 = __importDefault(require("dotenv"));
dotenv_1["default"].config();
var app = (0, express_1["default"])();
app.set("views", path_1["default"].join(__dirname, "views"));
app.set('view engine', 'pug');
app.get('/', function (req, res) {
    res.render('index-spa');
});
app.get("/auth_config.json", function (req, res) {
    res.json({
        "domain": "fer-web2.eu.auth0.com",
        "clientId": process.env.SPA_CLIENT_ID,
        "audience": 'FER-Web2 WebAPI'
    });
});
var port = 4092;
https_1["default"].createServer({
    key: fs_1["default"].readFileSync('server.key'),
    cert: fs_1["default"].readFileSync('server.cert')
}, app)
    .listen(port, function () {
    console.log("SPA running at https://localhost:".concat(port, "/"));
});
